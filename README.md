# Operator App Prototype using Leanback Support Library Showcase App for Android TV
Operator App prototype to portrays what an Operator App can deliver using YouTube videos as our content.

## Features supported
- Live Videos & Videos on Demand (VODs).
- Live Videos are demostrated like they're live TV channels. [Can use CH+/CH- buttons to change channels]
- VODs are demostrated as several categories; News & Sports, TV Shows, & Movies
- Red/Green/Blue/Yellow buttons are used as shortcut keys to each category respectively.
- Voice search can be used inside & outside the app.
- Can play/pause/rewind/fast forward/skip next/skip previous using their respective button on remote control.

## Live Video not Working -- Changing Live Video ID
As sometimes the associated YouTube channel stopped their live stream video and started a new one for some reasons, we need to update the related live video ID from time to time.
This can be recognized when an error "This live stream recording is not available" is shown when trying to play the live video.

### Steps to Change the Video ID:
	1. Target which video needed the change.
	2. Find its current YouTube video ID inside the source code.
	3. Find the new YouTube video ID from the YouTube website.
	4. Replace the old ID with the new one inside the source code. **(Note: use "replace all" feature inside Android Studio)**
	5. Run the latest source code to re-install the app inside the TV.

### Tutorial Video:
[![TUTORIAL VIDEO FOR CHANGING VIDEO ID](https://img.youtube.com/vi/suDWbHdUzmY/0.jpg)](https://www.youtube.com/watch?v=suDWbHdUzmY)

## Screenshots

![Main browser](Screenshots/new_main.png)
![Live TV Channels](Screenshots/new_livetv_focused.png)
![News & Sports](Screenshots/new_news_and_sports.png)
![VOD Details](Screenshots/new_details.png)
![VOD Episodes](Screenshots/new_episodes.png)
![Voice Search](Screenshots/new_search.png)



Need more information about using Leanback Support Library? Check the [official docs][getting-started].

## Support

If you need additional help regarding using Leanback Support Library, these community might be able to help.

- Android TV Google+ Community: [https://g.co/androidtvdev](https://g.co/androidtvdev)
- Stack Overflow: http://stackoverflow.com/questions/tagged/android-tv

## License

Licensed under the Apache 2.0 license. See the [LICENSE file][license] for details.

[store-apps]: https://play.google.com/store/apps/collection/promotion_3000e26_androidtv_apps_all
[studio]: https://developer.android.com/tools/studio/index.html
[getting-started]: https://developer.android.com/training/tv/start/start.html
[bugs]: https://github.com/googlesamples/androidtv-Leanback/issues/new
[contributing]: CONTRIBUTING.md
[license]: LICENSE
